1 root@CentOS6:~ > # df -h
Filesystem                      Size  Used Avail Use% Mounted on
/dev/mapper/vg_centos6-lv_root   38G  3.7G   33G  11% /
tmpfs                           3.9G   84K  3.9G   1% /dev/shm
/dev/sda1                       485M   40M  421M   9% /boot
/dev/mapper/vg_centos6-lv_opt    29G  172M   28G   1% /opt
/dev/mapper/vg_centos6-lv_tmp   3.9G   73M  3.6G   2% /tmp
/dev/sr0                        4.2G  4.2G     0 100% /media/CentOS_6.5_Final
2 root@CentOS6:~ > # 
2 root@CentOS6:~ > # 
2 root@CentOS6:~ > # fdisk -l
Disk /dev/sda: 85.9 GB, 85899345920 bytes
255 heads, 63 sectors/track, 10443 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x000b61ae

   Device Boot      Start         End      Blocks   Id  System
/dev/sda1   *           1          64      512000   83  Linux
Partition 1 does not end on cylinder boundary.
/dev/sda2              64       10444    83373056   8e  Linux LVM

#通过VMware直接添加一个10G的硬盘，/dev/sdb，用来给LVM的测试使用
Disk /dev/sdb: 10.7 GB, 10737418240 bytes
255 heads, 63 sectors/track, 1305 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000

#下面的各个LVM卷是安装系统时创建的
Disk /dev/mapper/vg_centos6-lv_root: 41.3 GB, 41330671616 bytes
255 heads, 63 sectors/track, 5024 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000


Disk /dev/mapper/vg_centos6-lv_swap: 8388 MB, 8388608000 bytes
255 heads, 63 sectors/track, 1019 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000


Disk /dev/mapper/vg_centos6-lv_tmp: 4194 MB, 4194304000 bytes
255 heads, 63 sectors/track, 509 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000


Disk /dev/mapper/vg_centos6-lv_opt: 31.5 GB, 31457280000 bytes
255 heads, 63 sectors/track, 3824 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000

3 root@CentOS6:~ > # 
3 root@CentOS6:~ > # 
3 root@CentOS6:~ > # 
3 root@CentOS6:~ > # 
#用/dev/sdb创建四个分区，每个2G，保留2G
3 root@CentOS6:~ > # fdisk /dev/sdb
Device contains neither a valid DOS partition table, nor Sun, SGI or OSF disklabel
Building a new DOS disklabel with disk identifier 0x4018a148.
Changes will remain in memory only, until you decide to write them.
After that, of course, the previous content won't be recoverable.

Warning: invalid flag 0x0000 of partition table 4 will be corrected by w(rite)

WARNING: DOS-compatible mode is deprecated. It's strongly recommended to
         switch off the mode (command 'c') and change display units to
         sectors (command 'u').

Command (m for help): 
Command (m for help): 
#n表示创建分区
Command (m for help): n
Command action
   e   extended
   p   primary partition (1-4)
p  
Partition number (1-4): 1
First cylinder (1-1305, default 1): 
Using default value 1
Last cylinder, +cylinders or +size{K,M,G} (1-1305, default 1305): +2G

Command (m for help): n
Command action
   e   extended
   p   primary partition (1-4)
p
Partition number (1-4): 2
First cylinder (263-1305, default 263): 
Using default value 263
Last cylinder, +cylinders or +size{K,M,G} (263-1305, default 1305): +2G

Command (m for help): n
Command action
   e   extended
   p   primary partition (1-4)
p
Partition number (1-4): 3
First cylinder (525-1305, default 525): 
Using default value 525
Last cylinder, +cylinders or +size{K,M,G} (525-1305, default 1305): +2G

Command (m for help): n
Command action
   e   extended
   p   primary partition (1-4)
4
Invalid partition number for type `4'
Command action
   e   extended
   p   primary partition (1-4)
p
Selected partition 4
First cylinder (787-1305, default 787): 
Using default value 787
Last cylinder, +cylinders or +size{K,M,G} (787-1305, default 1305): +2G

Command (m for help): 
Command (m for help): m
Command action
   a   toggle a bootable flag
   b   edit bsd disklabel
   c   toggle the dos compatibility flag
   d   delete a partition
   l   list known partition types
   m   print this menu
   n   add a new partition
   o   create a new empty DOS partition table
   p   print the partition table
   q   quit without saving changes
   s   create a new empty Sun disklabel
   t   change a partition's system id
   u   change display/entry units
   v   verify the partition table
   w   write table to disk and exit
   x   extra functionality (experts only)

#更改分区的system ID
Command (m for help): t
Partition number (1-4): 1
Hex code (type L to list codes): 8e
#注意看，改成8e后就变成了Linux LVM
Changed system type of partition 1 to 8e (Linux LVM)

Command (m for help): t
Partition number (1-4): 2
Hex code (type L to list codes): 8e
Changed system type of partition 2 to 8e (Linux LVM)

Command (m for help): t
Partition number (1-4): 3
Hex code (type L to list codes): 8e
Changed system type of partition 3 to 8e (Linux LVM)

Command (m for help): t
Partition number (1-4): 4
Hex code (type L to list codes): 8e
Changed system type of partition 4 to 8e (Linux LVM)

Command (m for help): w
The partition table has been altered!

Calling ioctl() to re-read partition table.
Syncing disks.
4 root@CentOS6:~ > # 
4 root@CentOS6:~ > # fdisk -l

Disk /dev/sda: 85.9 GB, 85899345920 bytes
255 heads, 63 sectors/track, 10443 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x000b61ae

   Device Boot      Start         End      Blocks   Id  System
/dev/sda1   *           1          64      512000   83  Linux
Partition 1 does not end on cylinder boundary.
/dev/sda2              64       10444    83373056   8e  Linux LVM

Disk /dev/sdb: 10.7 GB, 10737418240 bytes
255 heads, 63 sectors/track, 1305 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x4018a148

   Device Boot      Start         End      Blocks   Id  System
/dev/sdb1               1         262     2104483+  8e  Linux LVM
/dev/sdb2             263         524     2104515   8e  Linux LVM
/dev/sdb3             525         786     2104515   8e  Linux LVM
/dev/sdb4             787        1048     2104515   8e  Linux LVM

Disk /dev/mapper/vg_centos6-lv_root: 41.3 GB, 41330671616 bytes
255 heads, 63 sectors/track, 5024 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000


Disk /dev/mapper/vg_centos6-lv_swap: 8388 MB, 8388608000 bytes
255 heads, 63 sectors/track, 1019 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000


Disk /dev/mapper/vg_centos6-lv_tmp: 4194 MB, 4194304000 bytes
255 heads, 63 sectors/track, 509 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000


Disk /dev/mapper/vg_centos6-lv_opt: 31.5 GB, 31457280000 bytes
255 heads, 63 sectors/track, 3824 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000

# 未开始测试LVM前，只有一个安装系统时创建PV
4 root@CentOS6:~ > # pvscan
  PV /dev/sda2   VG vg_centos6   lvm2 [79.51 GiB / 0    free]
  Total: 1 [79.51 GiB] / in use: 1 [79.51 GiB] / in no VG: 0 [0   ]

5 root@CentOS6:~ > # 
# pvcreate /dev/sdb{1,2,3,4}效果一样
5 root@CentOS6:~ > # pvcreate /dev/sdb[1-4]
  Physical volume "/dev/sdb1" successfully created
  Physical volume "/dev/sdb2" successfully created
  Physical volume "/dev/sdb3" successfully created
  Physical volume "/dev/sdb4" successfully created
6 root@CentOS6:~ > # 
# pvscan显示所有PV信息，最后一行显示的是：整体PV的量，已被使用到VG的PV量，剩余尚未被使用的PV量
6 root@CentOS6:~ > # pvscan
  PV /dev/sda2   VG vg_centos6      lvm2 [79.51 GiB / 0    free]
  PV /dev/sdb1                      lvm2 [2.01 GiB]
  PV /dev/sdb2                      lvm2 [2.01 GiB]
  PV /dev/sdb3                      lvm2 [2.01 GiB]
  PV /dev/sdb4                      lvm2 [2.01 GiB]
  Total: 5 [87.54 GiB] / in use: 1 [79.51 GiB] / in no VG: 4 [8.03 GiB]
7 root@CentOS6:~ > # 
7 root@CentOS6:~ > # pvdisplay /dev/sdb1
  "/dev/sdb1" is a new physical volume of "2.01 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb1		# 实际的partition设备名称
  VG Name               						# 因为尚未被分配出去，所以这里不显示属于哪个VG
  PV Size               2.01 GiB		# PV的容量
  Allocatable           NO					# 是否已被分配
  PE Size               0   				# 此PV内PE的大小，由于PE只能在创建VG时指定或默认，所以这里显示为零。下面的PE同理
  Total PE              0						# 此PV内PE的个数
  Free PE               0						# 没被LV用掉的PE
  Allocated PE          0						# 还可以被分配出去的PE数量
  PV UUID               TUzvaD-qO5z-BRiv-s0vY-5qmJ-0wm1-4sPmPG
   
8 root@CentOS6:~ > # 
# 使用/dev/sdb[1-3]创建VG，剩余的sdb4留作测试扩容VG用
8 root@CentOS6:~ > # vgcreate -s 16M zcqvg /dev/sdb[123]
  Volume group "zcqvg" successfully created
9 root@CentOS6:~ > # vgscan
  Reading all physical volumes.  This may take a while...
  Found volume group "zcqvg" using metadata type lvm2				# 手工创建的VG
  Found volume group "vg_centos6" using metadata type lvm2	# 安装系统时创建的VG
10 root@CentOS6:~ > # 
10 root@CentOS6:~ > # pvscan
  PV /dev/sdb1   VG zcqvg           lvm2 [2.00 GiB / 2.00 GiB free]
  PV /dev/sdb2   VG zcqvg           lvm2 [2.00 GiB / 2.00 GiB free]
  PV /dev/sdb3   VG zcqvg           lvm2 [2.00 GiB / 2.00 GiB free]
  PV /dev/sda2   VG vg_centos6      lvm2 [79.51 GiB / 0    free]
  PV /dev/sdb4                      lvm2 [2.01 GiB]
  Total: 5 [87.51 GiB] / in use: 4 [85.51 GiB] / in no VG: 1 [2.01 GiB]
11 root@CentOS6:~ > # 
11 root@CentOS6:~ > # vgdisplay zcqvg
  --- Volume group ---
  VG Name               zcqvg
  System ID             
  Format                lvm2
  Metadata Areas        3
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                3
  Act PV                3
  VG Size               6.00 GiB				# 整体VG的大小
  PE Size               16.00 MiB				# 每个PE的大小
  Total PE              384							# VG内含有的PE总量
  Alloc PE / Size       0 / 0   
  Free  PE / Size       384 / 6.00 GiB	# 尚能被分配给LV的PE数量/PE的总容量
  VG UUID               lDNESd-puZ0-cwpP-YFdz-fI8y-Q7E5-YXDJQR
   
12 root@CentOS6:~ > # 
# 将剩余的sdb4扩容给VG
12 root@CentOS6:~ > # vgextend zcqvg /dev/sdb4
  Volume group "zcqvg" successfully extended
13 root@CentOS6:~ > # 
13 root@CentOS6:~ > # vgdisplay zcqvg
  --- Volume group ---
  VG Name               zcqvg
  System ID             
  Format                lvm2
  Metadata Areas        4
  Metadata Sequence No  2
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                4
  Act PV                4
  VG Size               8.00 GiB
  PE Size               16.00 MiB
  Total PE              512
  Alloc PE / Size       0 / 0   
  Free  PE / Size       512 / 8.00 GiB
  VG UUID               lDNESd-puZ0-cwpP-YFdz-fI8y-Q7E5-YXDJQR
   
14 root@CentOS6:~ > # 
14 root@CentOS6:~ > # df -h
Filesystem                      Size  Used Avail Use% Mounted on
/dev/mapper/vg_centos6-lv_root   38G  3.7G   33G  11% /
tmpfs                           3.9G   84K  3.9G   1% /dev/shm
/dev/sda1                       485M   40M  421M   9% /boot
/dev/mapper/vg_centos6-lv_opt    29G  172M   28G   1% /opt
/dev/mapper/vg_centos6-lv_tmp   3.9G   73M  3.6G   2% /tmp
/dev/sr0                        4.2G  4.2G     0 100% /media/CentOS_6.5_Final
15 root@CentOS6:~ > # 
15 root@CentOS6:~ > # fdisk -l

Disk /dev/sda: 85.9 GB, 85899345920 bytes
255 heads, 63 sectors/track, 10443 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x000b61ae

   Device Boot      Start         End      Blocks   Id  System
/dev/sda1   *           1          64      512000   83  Linux
Partition 1 does not end on cylinder boundary.
/dev/sda2              64       10444    83373056   8e  Linux LVM

Disk /dev/sdb: 10.7 GB, 10737418240 bytes
255 heads, 63 sectors/track, 1305 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x4018a148

   Device Boot      Start         End      Blocks   Id  System
/dev/sdb1               1         262     2104483+  8e  Linux LVM
/dev/sdb2             263         524     2104515   8e  Linux LVM
/dev/sdb3             525         786     2104515   8e  Linux LVM
/dev/sdb4             787        1048     2104515   8e  Linux LVM

Disk /dev/mapper/vg_centos6-lv_root: 41.3 GB, 41330671616 bytes
255 heads, 63 sectors/track, 5024 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000


Disk /dev/mapper/vg_centos6-lv_swap: 8388 MB, 8388608000 bytes
255 heads, 63 sectors/track, 1019 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000


Disk /dev/mapper/vg_centos6-lv_tmp: 4194 MB, 4194304000 bytes
255 heads, 63 sectors/track, 509 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000


Disk /dev/mapper/vg_centos6-lv_opt: 31.5 GB, 31457280000 bytes
255 heads, 63 sectors/track, 3824 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000

16 root@CentOS6:~ > # 
# 用VG:zcqvg创建一个2G的LV：zcqlv
16 root@CentOS6:~ > # lvcreate -L 2G -n zcqlv zcqvg
  Logical volume "zcqlv" created
17 root@CentOS6:~ > # 
17 root@CentOS6:~ > # lvscan
  ACTIVE            '/dev/zcqvg/zcqlv' [2.00 GiB] inherit						# 新增加的LV
  ACTIVE            '/dev/vg_centos6/lv_tmp' [3.91 GiB] inherit			# 以下是安装系统时创建的LV
  ACTIVE            '/dev/vg_centos6/lv_root' [38.49 GiB] inherit
  ACTIVE            '/dev/vg_centos6/lv_swap' [7.81 GiB] inherit
  ACTIVE            '/dev/vg_centos6/lv_opt' [29.30 GiB] inherit
18 root@CentOS6:~ > # 
# 通过lvdisplay查看zcqlv，注意要加全路径
18 root@CentOS6:~ > # lvdisplay zcqlv
  Volume group "zcqlv" not found
  Skipping volume group zcqlv
19 root@CentOS6:~ > # lvdisplay /dev/zcqvg/zcqlv
  --- Logical volume ---
  LV Path                /dev/zcqvg/zcqlv
  LV Name                zcqlv
  VG Name                zcqvg
  LV UUID                QxTC37-PI0i-PBvK-rAmy-6bpt-2Btd-nB31a1
  LV Write Access        read/write
  LV Creation host, time CentOS6, 2019-05-11 22:26:21 +0800
  LV Status              available
  # open                 0
  LV Size                2.00 GiB				# 此LV的总容量
  Current LE             128
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:4
   
20 root@CentOS6:~ > # 
# 查看文件系统类型
20 root@CentOS6:~ > # df -hT
Filesystem                     Type     Size  Used Avail Use% Mounted on
/dev/mapper/vg_centos6-lv_root ext4      38G  3.7G   33G  11% /
tmpfs                          tmpfs    3.9G   84K  3.9G   1% /dev/shm
/dev/sda1                      ext4     485M   40M  421M   9% /boot
/dev/mapper/vg_centos6-lv_opt  ext4      29G  172M   28G   1% /opt
/dev/mapper/vg_centos6-lv_tmp  ext4     3.9G   73M  3.6G   2% /tmp
/dev/sr0                       iso9660  4.2G  4.2G     0 100% /media/CentOS_6.5_Final
21 root@CentOS6:~ > # 
# 要使用该LV，需要先格式化，然后挂载
21 root@CentOS6:~ > # mkfs.ext4 /dev/zcqvg/zcqlv
mke2fs 1.41.12 (17-May-2010)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
131072 inodes, 524288 blocks
26214 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=536870912
16 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks: 
        32768, 98304, 163840, 229376, 294912

Writing inode tables: done                            
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done

This filesystem will be automatically checked every 21 mounts or
180 days, whichever comes first.  Use tune2fs -c or -i to override.
22 root@CentOS6:~ > # 
# 挂载到/home/zcqlvm下
22 root@CentOS6:~ > # cd /home
23 root@CentOS6:/home > # ls
ara  Aragorn  zcq
24 root@CentOS6:/home > # mkdir zcqlvm
25 root@CentOS6:/home > # 
25 root@CentOS6:/home > # mount /dev/zcqvg/zcqlv /home/zcqlvm
26 root@CentOS6:/home > # cd zcqlvm/
27 root@CentOS6:/home/zcqlvm > # df - h
df: `-': No such file or directory
df: `h': No such file or directory
df: no file systems processed
28 root@CentOS6:/home/zcqlvm > # df -h .
Filesystem               Size  Used Avail Use% Mounted on
/dev/mapper/zcqvg-zcqlv  2.0G   67M  1.9G   4% /home/zcqlvm
29 root@CentOS6:/home/zcqlvm > # 
# 扩容/home/zcqlvm的思路是给LV扩容，然后载入文件系统，注意此过程不需要umount或格式化文件系统
29 root@CentOS6:/home/zcqlvm > # lvresize -L +4G /dev/zcqvg/zcqlv
  Extending logical volume zcqlv to 6.00 GiB
  Logical volume zcqlv successfully resized
30 root@CentOS6:/home/zcqlvm > # 
# 查看LV的确已扩容
30 root@CentOS6:/home/zcqlvm > # lvscan
  ACTIVE            '/dev/zcqvg/zcqlv' [6.00 GiB] inherit
  ACTIVE            '/dev/vg_centos6/lv_tmp' [3.91 GiB] inherit
  ACTIVE            '/dev/vg_centos6/lv_root' [38.49 GiB] inherit
  ACTIVE            '/dev/vg_centos6/lv_swap' [7.81 GiB] inherit
  ACTIVE            '/dev/vg_centos6/lv_opt' [29.30 GiB] inherit
31 root@CentOS6:/home/zcqlvm > # 
# 但文件系统ext4显示尚未扩容
31 root@CentOS6:/home/zcqlvm > # df -hT
Filesystem                     Type     Size  Used Avail Use% Mounted on
/dev/mapper/vg_centos6-lv_root ext4      38G  3.7G   33G  11% /
tmpfs                          tmpfs    3.9G   84K  3.9G   1% /dev/shm
/dev/sda1                      ext4     485M   40M  421M   9% /boot
/dev/mapper/vg_centos6-lv_opt  ext4      29G  172M   28G   1% /opt
/dev/mapper/vg_centos6-lv_tmp  ext4     3.9G   73M  3.6G   2% /tmp
/dev/sr0                       iso9660  4.2G  4.2G     0 100% /media/CentOS_6.5_Final
/dev/mapper/zcqvg-zcqlv        ext4     2.0G   67M  1.9G   4% /home/zcqlvm
32 root@CentOS6:/home/zcqlvm > # 
# 尝试卸载重挂，无效
32 root@CentOS6:/home/zcqlvm > # unmount /home/zcqlvm
-bash: unmount: command not found
33 root@CentOS6:/home/zcqlvm > # umount /home/zcqlvm 
umount: /home/zcqlvm: device is busy.
        (In some cases useful info about processes that use
         the device is found by lsof(8) or fuser(1))
34 root@CentOS6:/home/zcqlvm > # ..
35 root@CentOS6:/home > # umount /home/zcqlvm
36 root@CentOS6:/home > # df -hT
Filesystem                     Type     Size  Used Avail Use% Mounted on
/dev/mapper/vg_centos6-lv_root ext4      38G  3.7G   33G  11% /
tmpfs                          tmpfs    3.9G   84K  3.9G   1% /dev/shm
/dev/sda1                      ext4     485M   40M  421M   9% /boot
/dev/mapper/vg_centos6-lv_opt  ext4      29G  172M   28G   1% /opt
/dev/mapper/vg_centos6-lv_tmp  ext4     3.9G   73M  3.6G   2% /tmp
/dev/sr0                       iso9660  4.2G  4.2G     0 100% /media/CentOS_6.5_Final
37 root@CentOS6:/home > # lvdisplay /dev/zcqvg/zcqlv
  --- Logical volume ---
  LV Path                /dev/zcqvg/zcqlv
  LV Name                zcqlv
  VG Name                zcqvg
  LV UUID                QxTC37-PI0i-PBvK-rAmy-6bpt-2Btd-nB31a1
  LV Write Access        read/write
  LV Creation host, time CentOS6, 2019-05-11 22:26:21 +0800
  LV Status              available
  # open                 0
  LV Size                6.00 GiB
  Current LE             384
  Segments               3
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:4
   
38 root@CentOS6:/home > # 
38 root@CentOS6:/home > # mount /dev/zcqvg/zcqlv /home/zcqlvm
39 root@CentOS6:/home > # 
39 root@CentOS6:/home > # df -hT /home/zcqlvm
Filesystem              Type  Size  Used Avail Use% Mounted on
/dev/mapper/zcqvg-zcqlv ext4  2.0G   67M  1.9G   4% /home/zcqlvm
40 root@CentOS6:/home > # 
# ext需要用resieze2fs，xfs需要用xfs_growfs，命令重新载入LV
40 root@CentOS6:/home > # resize2fs /dev/zcqvg/zcqlv
resize2fs 1.41.12 (17-May-2010)
Filesystem at /dev/zcqvg/zcqlv is mounted on /home/zcqlvm; on-line resizing required
old desc_blocks = 1, new_desc_blocks = 1
Performing an on-line resize of /dev/zcqvg/zcqlv to 1572864 (4k) blocks.
The filesystem on /dev/zcqvg/zcqlv is now 1572864 blocks long.

#重载后显示文件系统已扩容
41 root@CentOS6:/home > # df -hT /home/zcqlvm       
Filesystem              Type  Size  Used Avail Use% Mounted on
/dev/mapper/zcqvg-zcqlv ext4  6.0G   68M  5.6G   2% /home/zcqlvm
42 root@CentOS6:/home > # 
42 root@CentOS6:/home > # vgdisplay zcqvg
  --- Volume group ---
  VG Name               zcqvg
  System ID             
  Format                lvm2
  Metadata Areas        4
  Metadata Sequence No  4
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                1
  Open LV               1
  Max PV                0
  Cur PV                4
  Act PV                4
  VG Size               8.00 GiB
  PE Size               16.00 MiB
  Total PE              512
  Alloc PE / Size       384 / 6.00 GiB
  Free  PE / Size       128 / 2.00 GiB
  VG UUID               lDNESd-puZ0-cwpP-YFdz-fI8y-Q7E5-YXDJQR
   
43 root@CentOS6:/home > # 
# 继续尝试扩容
43 root@CentOS6:/home > # lvresize -L +2G /dev/zcqvg/zcqlv
  Extending logical volume zcqlv to 8.00 GiB
  Logical volume zcqlv successfully resized
44 root@CentOS6:/home > # lvscan /dev/zcqvg/zcqlv
  No additional command line arguments allowed
  Run `lvscan --help' for more information.
45 root@CentOS6:/home > # lvscan
  ACTIVE            '/dev/zcqvg/zcqlv' [8.00 GiB] inherit
  ACTIVE            '/dev/vg_centos6/lv_tmp' [3.91 GiB] inherit
  ACTIVE            '/dev/vg_centos6/lv_root' [38.49 GiB] inherit
  ACTIVE            '/dev/vg_centos6/lv_swap' [7.81 GiB] inherit
  ACTIVE            '/dev/vg_centos6/lv_opt' [29.30 GiB] inherit
46 root@CentOS6:/home > # lvdisplay /dev/zcqvg/zcqlv
  --- Logical volume ---
  LV Path                /dev/zcqvg/zcqlv
  LV Name                zcqlv
  VG Name                zcqvg
  LV UUID                QxTC37-PI0i-PBvK-rAmy-6bpt-2Btd-nB31a1
  LV Write Access        read/write
  LV Creation host, time CentOS6, 2019-05-11 22:26:21 +0800
  LV Status              available
  # open                 1
  LV Size                8.00 GiB
  Current LE             512
  Segments               4
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:4
   
47 root@CentOS6:/home > # df -hT /home/zcqlvm
Filesystem              Type  Size  Used Avail Use% Mounted on
/dev/mapper/zcqvg-zcqlv ext4  6.0G   68M  5.6G   2% /home/zcqlvm
48 root@CentOS6:/home > # 
48 root@CentOS6:/home > # resize2fs /dev/zcqvg/zcqlv
resize2fs 1.41.12 (17-May-2010)
Filesystem at /dev/zcqvg/zcqlv is mounted on /home/zcqlvm; on-line resizing required
old desc_blocks = 1, new_desc_blocks = 1
Performing an on-line resize of /dev/zcqvg/zcqlv to 2097152 (4k) blocks.
The filesystem on /dev/zcqvg/zcqlv is now 2097152 blocks long.

49 root@CentOS6:/home > # df -hT /home/zcqlvm       
Filesystem              Type  Size  Used Avail Use% Mounted on
/dev/mapper/zcqvg-zcqlv ext4  7.9G   69M  7.5G   1% /home/zcqlvm
50 root@CentOS6:/home > # 
50 root@CentOS6:/home > # df -hT
Filesystem                     Type     Size  Used Avail Use% Mounted on
/dev/mapper/vg_centos6-lv_root ext4      38G  3.7G   33G  11% /
tmpfs                          tmpfs    3.9G   84K  3.9G   1% /dev/shm
/dev/sda1                      ext4     485M   40M  421M   9% /boot
/dev/mapper/vg_centos6-lv_opt  ext4      29G  172M   28G   1% /opt
/dev/mapper/vg_centos6-lv_tmp  ext4     3.9G   73M  3.6G   2% /tmp
/dev/sr0                       iso9660  4.2G  4.2G     0 100% /media/CentOS_6.5_Final
/dev/mapper/zcqvg-zcqlv        ext4     7.9G   69M  7.5G   1% /home/zcqlvm
51 root@CentOS6:/home > # 